<?php

use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return static function (RoutingConfigurator $routes) {
    $routes->add('x_one_payu_notify', '/notify')
        ->controller('x_one_payu.controller.notify')
        ->methods(['POST']);
};
