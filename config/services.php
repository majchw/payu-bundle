<?php

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use XOne\Bundle\PayuBundle\Controller\NotifyController;
use XOne\Bundle\PayuBundle\Event\OrderNotificationEvent;
use XOne\Bundle\PayuBundle\EventListener\UpdateLocalReceiptDateTimeFromNotification;
use XOne\Bundle\PayuBundle\EventListener\UpdateOrderStatusFromNotification;
use XOne\Bundle\PayuBundle\Factory\OrderFactory;
use XOne\Bundle\PayuBundle\Factory\OrderFactoryInterface;
use XOne\Bundle\PayuBundle\Factory\RefundFactory;
use XOne\Bundle\PayuBundle\Factory\RefundFactoryInterface;
use XOne\Bundle\PayuBundle\Factory\SubscriptionFactory;
use XOne\Bundle\PayuBundle\Factory\SubscriptionFactoryInterface;
use XOne\Bundle\PayuBundle\Http\Client;
use XOne\Bundle\PayuBundle\Http\ClientConfig;
use XOne\Bundle\PayuBundle\Http\ClientConfigInterface;
use XOne\Bundle\PayuBundle\Http\ClientInterface;
use XOne\Bundle\PayuBundle\Http\OpenPayu;
use XOne\Bundle\PayuBundle\Http\OpenPayuInterface;
use XOne\Bundle\PayuBundle\Repository\OrderRepository;
use XOne\Bundle\PayuBundle\Repository\OrderRepositoryInterface;
use XOne\Bundle\PayuBundle\Repository\RefundRepository;
use XOne\Bundle\PayuBundle\Repository\RefundRepositoryInterface;
use XOne\Bundle\PayuBundle\Repository\SubscriptionRepository;
use XOne\Bundle\PayuBundle\Repository\SubscriptionRepositoryInterface;

use function Symfony\Component\DependencyInjection\Loader\Configurator\abstract_arg;
use function Symfony\Component\DependencyInjection\Loader\Configurator\param;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

return static function (ContainerConfigurator $configurator) {
    $services = $configurator->services();

    $services
        ->set('x_one_payu.factory.order', OrderFactory::class)
        ->args([param('x_one_payu.entity.order.class')])
        ->alias(OrderFactoryInterface::class, 'x_one_payu.factory.order')
    ;

    $services
        ->set('x_one_payu.factory.refund', RefundFactory::class)
        ->args([param('x_one_payu.entity.refund.class')])
        ->alias(RefundFactoryInterface::class, 'x_one_payu.factory.refund')
    ;

    $services
        ->set('x_one_payu.factory.subscription', SubscriptionFactory::class)
        ->args([param('x_one_payu.entity.subscription.class')])
        ->alias(SubscriptionFactoryInterface::class, 'x_one_payu.factory.subscription')
    ;

    $services
        ->set('x_one_payu.http.client.config', ClientConfig::class)
        ->args([
            '$environment' => abstract_arg('defined in bundle configuration, set in bundle class'),
            '$merchantPosId' => abstract_arg('defined in bundle configuration, set in bundle class'),
            '$signatureKey' => abstract_arg('defined in bundle configuration, set in bundle class'),
            '$oauthClientId' => abstract_arg('defined in bundle configuration, set in bundle class'),
            '$oauthClientSecret' => abstract_arg('defined in bundle configuration, set in bundle class'),
            '$oauthGrantType' => abstract_arg('defined in bundle configuration, set in bundle class'),
            '$oauthEmail' => abstract_arg('defined in bundle configuration, set in bundle class'),
            '$oauthExtCustomerId' => abstract_arg('defined in bundle configuration, set in bundle class'),
            '$continueRoute' => abstract_arg('defined in bundle configuration, set in bundle class'),
            '$notifyRoute' => abstract_arg('defined in bundle configuration, set in bundle class'),
            '$cacheDir' => param('kernel.cache_dir'),
        ])
        ->alias(ClientConfigInterface::class, 'x_one_payu.http.client.config')
    ;

    $services
        ->set('x_one_payu.http.wrapper', OpenPayu::class)
        ->alias(OpenPayuInterface::class, 'x_one_payu.http.wrapper')
    ;

    $services
        ->set('x_one_payu.http.client', Client::class)
        ->args([
            service('x_one_payu.http.client.config'),
            service('x_one_payu.repository.order'),
            service('event_dispatcher'),
            service('router.default'),
            service('request_stack'),
            service('x_one_payu.http.wrapper'),
        ])
        ->alias(ClientInterface::class, 'x_one_payu.http.client')
    ;

    $services
        ->set('x_one_payu.controller.notify', NotifyController::class)
        ->args([service('x_one_payu.http.client')])
        ->tag('controller.service_arguments')
    ;

    $services
        ->set('x_one_payu.repository.order', OrderRepository::class)
        ->tag('doctrine.repository_service')
        ->args([service('doctrine'), param('x_one_payu.entity.order.class')])
        ->alias(OrderRepositoryInterface::class, 'x_one_payu.repository.order')
    ;

    $services
        ->set('x_one_payu.repository.refund', RefundRepository::class)
        ->tag('doctrine.repository_service')
        ->args([service('doctrine'), param('x_one_payu.entity.refund.class')])
        ->alias(RefundRepositoryInterface::class, 'x_one_payu.repository.refund')
    ;

    $services
        ->set('x_one_payu.repository.subscription', SubscriptionRepository::class)
        ->tag('doctrine.repository_service')
        ->args([service('doctrine'), param('x_one_payu.entity.subscription.class')])
        ->alias(SubscriptionRepositoryInterface::class, 'x_one_payu.repository.subscription')
    ;

    $services
        ->set('x_one_payu.event_listener.update_order_status_from_notification', UpdateOrderStatusFromNotification::class)
        ->args([
            service('x_one_payu.repository.order'),
            service('event_dispatcher'),
        ])
        ->tag('kernel.event_listener', [
            'event' => OrderNotificationEvent::class,
            'priority' => 1_000,
        ])
    ;

    $services
        ->set('x_one_payu.event_listener.update_local_receipt_date_time_from_notification', UpdateLocalReceiptDateTimeFromNotification::class)
        ->args([service('x_one_payu.repository.order')])
        ->tag('kernel.event_listener', [
            'event' => OrderNotificationEvent::class,
            'priority' => 1_000,
        ])
    ;
};
