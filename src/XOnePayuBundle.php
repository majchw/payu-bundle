<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle;

use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;
use XOne\Bundle\PayuBundle\Model\OrderInterface;
use XOne\Bundle\PayuBundle\Model\RefundInterface;
use XOne\Bundle\PayuBundle\Model\SubscriptionInterface;

class XOnePayuBundle extends AbstractBundle
{
    public function configure(DefinitionConfigurator $definition): void
    {
        $definition->import('../config/definition.php');
    }

    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $container->import('../config/services.php');

        $container->parameters()
            ->set('x_one_payu.entity.order.class', $config['entities']['order'])
            ->set('x_one_payu.entity.refund.class', $config['entities']['refund'])
            ->set('x_one_payu.entity.subscription.class', $config['entities']['subscription'])
        ;

        $builder->getDefinition('x_one_payu.http.client.config')
            ->setArgument('$environment', $config['api']['environment'])
            ->setArgument('$merchantPosId', $config['api']['merchant_pos_id'])
            ->setArgument('$signatureKey', $config['api']['signature_key'])
            ->setArgument('$oauthClientId', $config['api']['oauth_client_id'])
            ->setArgument('$oauthClientSecret', $config['api']['oauth_client_secret'])
            ->setArgument('$oauthGrantType', $config['api']['oauth_grant_type'])
            ->setArgument('$oauthEmail', $config['api']['oauth_email'] ?? null)
            ->setArgument('$oauthExtCustomerId', $config['api']['oauth_ext_customer_id'] ?? null)
            ->setArgument('$continueRoute', $config['api']['continue_route'])
            ->setArgument('$notifyRoute', $config['api']['notify_route'])
        ;
    }

    public function prependExtension(ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $config = $builder->getExtensionConfig('x_one_payu')[0];

        $container->extension('doctrine', [
            'orm' => [
                'resolve_target_entities' => array_filter([
                    OrderInterface::class => $config['entities']['order'] ?? null,
                    RefundInterface::class => $config['entities']['refund'] ?? null,
                    SubscriptionInterface::class => $config['entities']['subscription'] ?? null,
                ]),
            ],
        ]);
    }
}
