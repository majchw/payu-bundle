<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Event;

use XOne\Bundle\PayuBundle\Model\OrderInterface;

class OrderEvent
{
    public function __construct(
        private OrderInterface $order,
    ) {
    }

    public function getOrder(): OrderInterface
    {
        return $this->order;
    }
}
