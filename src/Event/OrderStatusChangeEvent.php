<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Event;

use XOne\Bundle\PayuBundle\Model\OrderInterface;
use XOne\Bundle\PayuBundle\Model\OrderStatus;

class OrderStatusChangeEvent extends OrderEvent
{
    public function __construct(
        OrderInterface $order,
        private OrderStatus $previousStatus,
    ) {
        parent::__construct($order);
    }

    public function getPreviousStatus(): OrderStatus
    {
        return $this->previousStatus;
    }
}
