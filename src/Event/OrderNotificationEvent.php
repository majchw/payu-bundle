<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Event;

use XOne\Bundle\PayuBundle\Model\OrderInterface;

class OrderNotificationEvent extends OrderEvent
{
    public function __construct(
        OrderInterface $order,
        private \OpenPayU_Result $openPayuResult,
    ) {
        parent::__construct($order);
    }

    public function getOpenPayuResult(): \OpenPayU_Result
    {
        return $this->openPayuResult;
    }
}
