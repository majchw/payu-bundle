<?php

namespace XOne\Bundle\PayuBundle\Model;

use Doctrine\Common\Collections\Collection;

interface OrderInterface
{
    public function getId(): mixed;

    public function getPayuId(): ?string;

    public function setPayuId(?string $payuId): static;

    public function getStatus(): OrderStatus;

    public function setStatus(string|OrderStatus $status): static;

    public function isNew(): bool;

    public function isPending(): bool;

    public function isWaitingForConfirmation(): bool;

    public function isCompleted(): bool;

    public function isCanceled(): bool;

    public function getDescription(): string;

    public function setDescription(?string $description): static;

    public function getAdditionalDescription(): ?string;

    public function setAdditionalDescription(?string $additionalDescription): static;

    /**
     * Order/Payment description visible for Buyer on the PayU payment page.
     *
     * Allowed length <= 80
     */
    public function getVisibleDescription(): ?string;

    public function setVisibleDescription(?string $visibleDescription): static;

    /**
     * Payment recipient name followed by payment description (order ID, ticket number etc) visible on card statement.
     * The name should be easy to recognize by the cardholder. If field is not provided, static name configured by PayU will be used.
     *
     * Allowed length <= 22
     */
    public function getStatementDescription(): ?string;

    public function setStatementDescription(?string $statementDescription): static;

    /**
     * Currency code compliant with ISO 4217 (e.g. EUR).
     */
    public function getCurrencyCode(): string;

    public function setCurrencyCode(?string $currencyCode): static;

    /**
     * Total price of the order in pennies (e.g. 1000 is 10.00 EUR).
     * Applies also to currencies without subunits (e.g. 1000 is 10 HUF).
     */
    public function getTotalAmount(): int;

    public function setTotalAmount(?int $totalAmount): static;

    public function getRedirectUri(): ?string;

    public function setRedirectUri(string $redirectUri): static;

    public function getBuyerFirstName(): ?string;

    public function setBuyerFirstName(?string $buyerFirstName): static;

    public function getBuyerLastName(): ?string;

    public function setBuyerLastName(?string $buyerLastName): static;

    public function getBuyerEmail(): ?string;

    public function setBuyerEmail(?string $buyerEmail): static;

    public function getBuyerLanguage(): ?string;

    public function setBuyerLanguage(?string $buyerLanguage): static;

    public function getLocalReceiptDateTime(): ?\DateTimeImmutable;

    public function setLocalReceiptDateTime(?\DateTimeImmutable $localReceiptDateTime): static;

    /**
     * @return Collection<RefundInterface>
     */
    public function getRefunds(): Collection;

    public function getRefundById(mixed $id): ?RefundInterface;

    public function getRefundByPayuId(string $payuId): ?RefundInterface;

    public function addRefund(RefundInterface $refund): static;

    public function removeRefund(RefundInterface $refund): static;

    public function getSubscription(): ?SubscriptionInterface;

    public function setSubscription(?SubscriptionInterface $subscription): static;
}
