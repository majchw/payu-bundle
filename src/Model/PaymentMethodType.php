<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Model;

enum PaymentMethodType: string
{
    case PBL = 'PBL';
    case CardToken = 'CARD_TOKEN';
    case PaymentWall = 'PAYMENT_WALL';
    case BlikAuthorizationCode = 'BLIK_AUTHORIZATION_CODE';
    case BlikToken = 'BLIK_TOKEN';
}
