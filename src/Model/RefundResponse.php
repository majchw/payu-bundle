<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Model;

class RefundResponse
{
    public function __construct(
        private string $id,
        private string $payuId,
        private int $amount,
        private string $currencyCode,
        private string $description,
        private string $status,
        private ?string $statusErrorCode = null,
        private ?string $statusErrorDescription = null,
    ) {
    }

    public static function fromResponse(object $refund): self
    {
        return new self(
            id: $refund->extRefundId,
            payuId: $refund->refundId,
            amount: (int) $refund->amount,
            currencyCode: $refund->currencyCode,
            description: $refund->description,
            status: $refund->status,
            statusErrorCode: $refund->statusError?->code ?? null,
            statusErrorDescription: $refund->statusError?->description ?? null,
        );
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getPayuId(): string
    {
        return $this->payuId;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function isPending(): bool
    {
        return RefundStatus::Pending->value === $this->status;
    }

    public function isFinalized(): bool
    {
        return RefundStatus::Finalized->value === $this->status;
    }

    public function isCanceled(): bool
    {
        return RefundStatus::Canceled->value === $this->status;
    }

    public function getStatusErrorCode(): ?string
    {
        return $this->statusErrorCode;
    }

    public function getStatusErrorDescription(): ?string
    {
        return $this->statusErrorDescription;
    }
}
