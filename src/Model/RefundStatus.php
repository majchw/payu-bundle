<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Model;

use Symfony\Contracts\Translation\TranslatableInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

enum RefundStatus: string implements TranslatableInterface
{
    case Pending = 'PENDING';
    case Finalized = 'FINALIZED';
    case Canceled = 'CANCELED';

    public function trans(TranslatorInterface $translator, ?string $locale = null): string
    {
        return $translator->trans(sprintf('refund_status.%s', strtolower($this->value)), domain: 'XOnePayuBundle', locale: $locale);
    }
}
