<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Model;

use Doctrine\Common\Collections\Collection;

interface SubscriptionInterface
{
    public function getId(): mixed;

    public function getFirstCardToken(): string;

    public function setFirstCardToken(string $firstCardToken): static;

    public function getReusableCardToken(): ?string;

    public function setReusableCardToken(?string $reusableCardToken): static;

    public function getFrequencyType(): SubscriptionFrequencyType;

    public function setFrequencyType(SubscriptionFrequencyType $frequencyType): static;

    public function getFrequencyValue(): int;

    public function setFrequencyValue(int $frequencyValue): static;

    public function getCardNumber(): ?string;

    public function setCardNumber(?string $cardNumber): static;

    public function getCardExpirationMonth(): ?int;

    public function setCardExpirationMonth(?int $cardExpirationMonth): static;

    public function getCardExpirationYear(): ?int;

    public function setCardExpirationYear(?int $cardExpirationYear): static;

    /**
     * Returns the card expiration month and year as a string formatted in a commonly used format, e.g. "01/29".
     */
    public function getCardExpirationDateFormattedString(): ?string;

    /**
     * @return Collection<OrderInterface>
     */
    public function getOrders(): Collection;

    public function addOrder(OrderInterface $order): static;

    public function removeOrder(OrderInterface $order): static;
}
