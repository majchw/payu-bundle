<?php

namespace XOne\Bundle\PayuBundle\Model;

interface RefundInterface
{
    public function getId(): mixed;

    public function getPayuId(): ?string;

    public function setPayuId(?string $payuId): static;

    public function getOrder(): OrderInterface;

    public function setOrder(OrderInterface $order): static;

    public function getStatus(): RefundStatus;

    public function setStatus(string|RefundStatus $status): static;

    /**
     * Error code if status {@see RefundInterface::getStatus()} equals {@see RefundStatus::Canceled}.
     */
    public function getStatusErrorCode(): ?string;

    public function setStatusErrorCode(?string $statusErrorCode): static;

    /**
     * Error message if status {@see RefundInterface::getStatus()} equals {@see RefundStatus::Canceled}.
     */
    public function getStatusErrorDescription(): ?string;

    public function setStatusErrorDescription(?string $statusErrorDescription): static;

    public function getDescription(): string;

    public function setDescription(?string $description): static;

    /**
     * Total amount of the refund in pennies (e.g. 1000 is 10.00 EUR).
     * Applies also to currencies without subunits (e.g. 1000 is 10 HUF).
     *
     * If amount is empty, that means the refund covers the entire amount of the order.
     */
    public function getAmount(): ?int;

    public function setAmount(?int $amount): static;

    /**
     * Currency code compliant with ISO 4217 (e.g. EUR).
     */
    public function getCurrencyCode(): string;

    public function setCurrencyCode(?string $currencyCode): static;

    public function updateStatusFromRefundResponse(RefundResponse $refundResponse): static;
}
