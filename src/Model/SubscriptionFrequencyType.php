<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Model;

use Symfony\Contracts\Translation\TranslatableInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

enum SubscriptionFrequencyType: string implements TranslatableInterface
{
    case Daily = 'daily';
    case Monthly = 'monthly';

    public function getTranslationKey(): string
    {
        return sprintf('subscription_frequency_type.%s', $this->value);
    }

    public function trans(TranslatorInterface $translator, ?string $locale = null): string
    {
        return $translator->trans($this->getTranslationKey(), ['value' => 1], 'XOnePayuBundle', $locale);
    }
}
