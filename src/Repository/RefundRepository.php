<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use XOne\Bundle\PayuBundle\Entity\Refund;
use XOne\Bundle\PayuBundle\Model\RefundInterface;

class RefundRepository extends ServiceEntityRepository implements RefundRepositoryInterface
{
    public function __construct(ManagerRegistry $registry, string $entityClass = Refund::class)
    {
        parent::__construct($registry, $entityClass);
    }

    public function save(RefundInterface $refund): void
    {
        $this->getEntityManager()->persist($refund);
        $this->getEntityManager()->flush();
    }
}
