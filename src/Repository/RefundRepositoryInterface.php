<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Repository;

use Doctrine\Persistence\ObjectRepository;
use XOne\Bundle\PayuBundle\Model\RefundInterface;

/**
 * @extends ObjectRepository<RefundInterface>
 *
 * @method RefundInterface|null find($id, $lockMode = null, $lockVersion = null)
 * @method RefundInterface|null findOneBy(array $criteria, array $orderBy = null)
 * @method RefundInterface[]    findAll()
 * @method RefundInterface[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
interface RefundRepositoryInterface extends ObjectRepository
{
    public function save(RefundInterface $refund): void;
}
