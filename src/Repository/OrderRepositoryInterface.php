<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Repository;

use Doctrine\Persistence\ObjectRepository;
use XOne\Bundle\PayuBundle\Model\OrderInterface;

/**
 * @extends ObjectRepository<OrderInterface>
 *
 * @method OrderInterface|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderInterface|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderInterface[]    findAll()
 * @method OrderInterface[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
interface OrderRepositoryInterface extends ObjectRepository
{
    public function save(OrderInterface $order): void;

    /**
     * @return OrderInterface[]
     */
    public function findHavingPendingRefunds(): array;
}
