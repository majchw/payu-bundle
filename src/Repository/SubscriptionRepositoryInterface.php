<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Repository;

use Doctrine\Persistence\ObjectRepository;
use XOne\Bundle\PayuBundle\Model\SubscriptionInterface;

/**
 * @extends ObjectRepository<SubscriptionInterface>
 *
 * @method SubscriptionInterface|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubscriptionInterface|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubscriptionInterface[]    findAll()
 * @method SubscriptionInterface[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
interface SubscriptionRepositoryInterface extends ObjectRepository
{
    public function save(SubscriptionInterface $subscription): void;
}
