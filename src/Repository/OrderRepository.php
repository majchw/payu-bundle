<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use XOne\Bundle\PayuBundle\Entity\Order;
use XOne\Bundle\PayuBundle\Model\OrderInterface;
use XOne\Bundle\PayuBundle\Model\RefundStatus;

class OrderRepository extends ServiceEntityRepository implements OrderRepositoryInterface
{
    public function __construct(ManagerRegistry $registry, string $entityClass = Order::class)
    {
        parent::__construct($registry, $entityClass);
    }

    public function save(OrderInterface $order): void
    {
        $this->getEntityManager()->persist($order);
        $this->getEntityManager()->flush();
    }

    public function findHavingPendingRefunds(): array
    {
        return $this->createQueryBuilder('payuOrder')
            ->innerJoin('payuOrder.refunds', 'payuRefund')
            ->where('payuRefund.status = :pendingStatus')
            ->setParameter('pendingStatus', RefundStatus::Pending->value)
            ->getQuery()
            ->getResult()
        ;
    }
}
