<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use XOne\Bundle\PayuBundle\Entity\Subscription;
use XOne\Bundle\PayuBundle\Model\SubscriptionInterface;

class SubscriptionRepository extends ServiceEntityRepository implements SubscriptionRepositoryInterface
{
    public function __construct(ManagerRegistry $registry, string $entityClass = Subscription::class)
    {
        parent::__construct($registry, $entityClass);
    }

    public function save(SubscriptionInterface $subscription): void
    {
        $this->getEntityManager()->persist($subscription);
        $this->getEntityManager()->flush();
    }
}
