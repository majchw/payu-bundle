<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\EventListener;

use XOne\Bundle\PayuBundle\Event\OrderNotificationEvent;
use XOne\Bundle\PayuBundle\Repository\OrderRepositoryInterface;

class UpdateLocalReceiptDateTimeFromNotification
{
    public function __construct(
        private OrderRepositoryInterface $orderRepository,
    ) {
    }

    public function __invoke(OrderNotificationEvent $event): void
    {
        $order = $event->getOrder();
        $response = $event->getOpenPayuResult()->getResponse();

        if (null === $localReceiptDateTime = $this->getLocalReceiptDateTimeFromResponse($response)) {
            return;
        }

        $order->setLocalReceiptDateTime($localReceiptDateTime);

        $this->orderRepository->save($order);
    }

    private function getLocalReceiptDateTimeFromResponse(object $response): ?\DateTimeImmutable
    {
        $localReceiptDateTime = $response->localReceiptDateTime ?? null;

        if (null === $localReceiptDateTime) {
            return null;
        }

        $localReceiptDateTime = \DateTimeImmutable::createFromFormat('Y-m-d\TH:i:s.uP', $localReceiptDateTime);

        if (false === $localReceiptDateTime) {
            return null;
        }

        return $localReceiptDateTime;
    }
}
