<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\EventListener;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use XOne\Bundle\PayuBundle\Event\OrderNotificationEvent;
use XOne\Bundle\PayuBundle\Event\OrderStatusChangeEvent;
use XOne\Bundle\PayuBundle\Model\OrderStatus;
use XOne\Bundle\PayuBundle\Repository\OrderRepositoryInterface;

class UpdateOrderStatusFromNotification
{
    public function __construct(
        private OrderRepositoryInterface $orderRepository,
        private EventDispatcherInterface $eventDispatcher,
    ) {
    }

    public function __invoke(OrderNotificationEvent $event): void
    {
        $order = $event->getOrder();
        $response = $event->getOpenPayuResult()->getResponse();

        $status = $response->order->status ?? null;

        if (!is_string($status)) {
            return;
        }

        $status = OrderStatus::tryFrom($status);
        $currentStatus = $order->getStatus();

        if (null === $status || $status === $currentStatus) {
            return;
        }

        $order->setStatus($status);

        $this->orderRepository->save($order);

        $this->eventDispatcher->dispatch(new OrderStatusChangeEvent($order, $currentStatus));
    }
}
