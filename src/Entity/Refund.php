<?php

namespace XOne\Bundle\PayuBundle\Entity;

use Symfony\Component\Uid\Uuid;
use XOne\Bundle\PayuBundle\Exception\InvalidArgumentException;
use XOne\Bundle\PayuBundle\Model\OrderInterface;
use XOne\Bundle\PayuBundle\Model\RefundInterface;
use XOne\Bundle\PayuBundle\Model\RefundResponse;
use XOne\Bundle\PayuBundle\Model\RefundStatus;

class Refund implements RefundInterface
{
    protected ?Uuid $id = null;
    protected ?string $payuId = null;
    protected ?OrderInterface $order = null;
    protected ?string $status = RefundStatus::Pending->value;
    protected ?string $statusErrorCode = null;
    protected ?string $statusErrorDescription = null;
    protected ?string $description = 'Zwrot';
    protected ?string $currencyCode = 'PLN';
    protected ?int $amount = null;

    public function __construct()
    {
        $this->id = Uuid::v4();
    }

    public function __toString(): string
    {
        return $this->payuId;
    }

    public function updateStatusFromRefundResponse(RefundResponse $refundResponse): static
    {
        $this->setStatus(RefundStatus::from($refundResponse->getStatus()));
        $this->setStatusErrorCode($refundResponse->getStatusErrorCode());
        $this->setStatusErrorDescription($refundResponse->getStatusErrorDescription());

        return $this;
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getPayuId(): ?string
    {
        return $this->payuId;
    }

    public function setPayuId(?string $payuId): static
    {
        $this->payuId = $payuId;

        return $this;
    }

    public function getOrder(): OrderInterface
    {
        if (null === $this->order) {
            throw new InvalidArgumentException('The refund\'s order is not set');
        }

        return $this->order;
    }

    public function setOrder(OrderInterface $order): static
    {
        $previousOrder = $this->order;

        $this->order = $order;

        $previousOrder?->removeRefund($this);

        $order->addRefund($this);

        return $this;
    }

    public function getStatus(): RefundStatus
    {
        return RefundStatus::from($this->status);
    }

    public function setStatus(string|RefundStatus $status): static
    {
        if (is_string($status)) {
            $status = RefundStatus::from($status);
        }

        $this->status = $status->value;

        return $this;
    }

    public function isPending(): bool
    {
        return RefundStatus::Pending->value === $this->status;
    }

    public function isFinalized(): bool
    {
        return RefundStatus::Finalized->value === $this->status;
    }

    public function isCanceled(): bool
    {
        return RefundStatus::Canceled->value === $this->status;
    }

    public function getStatusErrorCode(): ?string
    {
        return $this->statusErrorCode;
    }

    public function setStatusErrorCode(?string $statusErrorCode): static
    {
        $this->statusErrorCode = $statusErrorCode;

        return $this;
    }

    public function getStatusErrorDescription(): ?string
    {
        return $this->statusErrorDescription;
    }

    public function setStatusErrorDescription(?string $statusErrorDescription): static
    {
        $this->statusErrorDescription = $statusErrorDescription;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }

    public function setCurrencyCode(?string $currencyCode): static
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(?int $amount): static
    {
        $this->amount = $amount;

        return $this;
    }
}
