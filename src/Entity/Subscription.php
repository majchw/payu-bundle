<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Uid\Uuid;
use XOne\Bundle\PayuBundle\Exception\BadMethodCallException;
use XOne\Bundle\PayuBundle\Model\OrderInterface;
use XOne\Bundle\PayuBundle\Model\SubscriptionFrequencyType;
use XOne\Bundle\PayuBundle\Model\SubscriptionInterface;

class Subscription implements SubscriptionInterface
{
    protected Uuid $id;
    protected string $firstCardToken;
    protected ?string $reusableCardToken = null;
    protected SubscriptionFrequencyType $frequencyType;
    protected int $frequencyValue;
    protected ?string $cardNumber = null;
    protected ?int $cardExpirationMonth = null;
    protected ?int $cardExpirationYear = null;
    protected Collection $orders;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->orders = new ArrayCollection();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getFirstCardToken(): string
    {
        if (!isset($this->firstCardToken)) {
            throw new BadMethodCallException(sprintf('The first card token is not set, use the "%s::setFirstCardToken()" method."', $this::class));
        }

        return $this->firstCardToken;
    }

    public function setFirstCardToken(string $firstCardToken): static
    {
        $this->firstCardToken = $firstCardToken;

        return $this;
    }

    public function getReusableCardToken(): ?string
    {
        return $this->reusableCardToken;
    }

    public function setReusableCardToken(?string $reusableCardToken): static
    {
        $this->reusableCardToken = $reusableCardToken;

        return $this;
    }

    public function getFrequencyType(): SubscriptionFrequencyType
    {
        if (!isset($this->frequencyType)) {
            throw new BadMethodCallException(sprintf('The frequency type is not set, use the "%s::setFrequencyType()" method."', $this::class));
        }

        return $this->frequencyType;
    }

    public function setFrequencyType(SubscriptionFrequencyType $frequencyType): static
    {
        $this->frequencyType = $frequencyType;

        return $this;
    }

    public function getFrequencyValue(): int
    {
        if (!isset($this->frequencyValue)) {
            throw new BadMethodCallException(sprintf('The frequency value is not set, use the "%s::setFrequencyValue()" method."', $this::class));
        }

        return $this->frequencyValue;
    }

    public function setFrequencyValue(int $frequencyValue): static
    {
        $this->frequencyValue = $frequencyValue;

        return $this;
    }

    public function getCardNumber(): ?string
    {
        return $this->cardNumber;
    }

    public function setCardNumber(?string $cardNumber): static
    {
        $this->cardNumber = $cardNumber;

        return $this;
    }

    public function getCardExpirationMonth(): ?int
    {
        return $this->cardExpirationMonth;
    }

    public function setCardExpirationMonth(?int $cardExpirationMonth): static
    {
        $this->cardExpirationMonth = $cardExpirationMonth;

        return $this;
    }

    public function getCardExpirationYear(): ?int
    {
        return $this->cardExpirationYear;
    }

    public function setCardExpirationYear(?int $cardExpirationYear): static
    {
        $this->cardExpirationYear = $cardExpirationYear;

        return $this;
    }

    public function getCardExpirationDateFormattedString(): ?string
    {
        if (null === $this->cardExpirationMonth || null === $this->cardExpirationYear) {
            return null;
        }

        return vsprintf('%s/%s', [
            sprintf('%02d', (string) $this->cardExpirationMonth),
            substr((string) $this->cardExpirationYear, -2),
        ]);
    }

    /**
     * @return Collection<OrderInterface>
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(OrderInterface $order): static
    {
        if (!$this->orders->contains($order)) {
            $this->orders->add($order);
        }

        if ($this !== $order->getSubscription()) {
            $order->setSubscription($this);
        }

        return $this;
    }

    public function removeOrder(OrderInterface $order): static
    {
        if ($this->orders->contains($order)) {
            $this->orders->removeElement($order);
        }

        if ($this === $order->getSubscription()) {
            $order->setSubscription(null);
        }

        return $this;
    }
}
