<?php

namespace XOne\Bundle\PayuBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Uid\Uuid;
use XOne\Bundle\PayuBundle\Exception\BadMethodCallException;
use XOne\Bundle\PayuBundle\Model\OrderInterface;
use XOne\Bundle\PayuBundle\Model\OrderStatus;
use XOne\Bundle\PayuBundle\Model\RefundInterface;
use XOne\Bundle\PayuBundle\Model\SubscriptionInterface;

class Order implements OrderInterface
{
    protected Uuid $id;
    protected ?string $payuId = null;
    protected ?string $status = OrderStatus::New->value;
    protected string $description;
    protected ?string $additionalDescription = null;
    protected ?string $visibleDescription = null;
    protected ?string $statementDescription = null;
    protected string $currencyCode = 'PLN';
    protected int $totalAmount;
    protected ?string $redirectUri = null;
    protected ?string $buyerFirstName = null;
    protected ?string $buyerLastName = null;
    protected ?string $buyerEmail = null;
    protected ?string $buyerLanguage = null;
    protected ?\DateTimeImmutable $localReceiptDateTime = null;

    /**
     * @var Collection<RefundInterface>
     */
    protected Collection $refunds;
    protected ?SubscriptionInterface $subscription = null;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->refunds = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->payuId;
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getPayuId(): ?string
    {
        return $this->payuId;
    }

    public function setPayuId(?string $payuId): static
    {
        $this->payuId = $payuId;

        return $this;
    }

    public function getStatus(): OrderStatus
    {
        return OrderStatus::from($this->status);
    }

    public function setStatus(string|OrderStatus $status): static
    {
        if (is_string($status)) {
            $status = OrderStatus::from($status);
        }

        $this->status = $status->value;

        return $this;
    }

    public function isNew(): bool
    {
        return OrderStatus::New->value === $this->status;
    }

    public function isPending(): bool
    {
        return OrderStatus::Pending->value === $this->status;
    }

    public function isWaitingForConfirmation(): bool
    {
        return OrderStatus::WaitingForConfirmation->value === $this->status;
    }

    public function isCompleted(): bool
    {
        return OrderStatus::Completed->value === $this->status;
    }

    public function isCanceled(): bool
    {
        return OrderStatus::Canceled->value === $this->status;
    }

    public function getDescription(): string
    {
        if (!isset($this->description)) {
            throw new BadMethodCallException(sprintf('The description is not set, use the "%s::setDescription()" method."', $this::class));
        }

        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getAdditionalDescription(): ?string
    {
        return $this->additionalDescription;
    }

    public function setAdditionalDescription(?string $additionalDescription): static
    {
        $this->additionalDescription = $additionalDescription;

        return $this;
    }

    public function getVisibleDescription(): ?string
    {
        return $this->visibleDescription;
    }

    public function setVisibleDescription(?string $visibleDescription): static
    {
        $this->visibleDescription = $visibleDescription;

        return $this;
    }

    public function getStatementDescription(): ?string
    {
        return $this->statementDescription;
    }

    public function setStatementDescription(?string $statementDescription): static
    {
        $this->statementDescription = $statementDescription;

        return $this;
    }

    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }

    public function setCurrencyCode(?string $currencyCode): static
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    public function getTotalAmount(): int
    {
        if (!isset($this->totalAmount)) {
            throw new BadMethodCallException(sprintf('The total amount is not set, use the "%s::setTotalAmount()" method."', $this::class));
        }

        return $this->totalAmount;
    }

    public function setTotalAmount(?int $totalAmount): static
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    public function getRedirectUri(): ?string
    {
        return $this->redirectUri;
    }

    public function setRedirectUri(string $redirectUri): static
    {
        $this->redirectUri = $redirectUri;

        return $this;
    }

    public function getBuyerFirstName(): ?string
    {
        return $this->buyerFirstName;
    }

    public function setBuyerFirstName(?string $buyerFirstName): static
    {
        $this->buyerFirstName = $buyerFirstName;

        return $this;
    }

    public function getBuyerLastName(): ?string
    {
        return $this->buyerLastName;
    }

    public function setBuyerLastName(?string $buyerLastName): static
    {
        $this->buyerLastName = $buyerLastName;

        return $this;
    }

    public function getBuyerEmail(): ?string
    {
        return $this->buyerEmail;
    }

    public function setBuyerEmail(?string $buyerEmail): static
    {
        $this->buyerEmail = $buyerEmail;

        return $this;
    }

    public function getBuyerLanguage(): ?string
    {
        return $this->buyerLanguage;
    }

    public function setBuyerLanguage(?string $buyerLanguage): static
    {
        $this->buyerLanguage = $buyerLanguage;

        return $this;
    }

    public function getLocalReceiptDateTime(): ?\DateTimeImmutable
    {
        return $this->localReceiptDateTime;
    }

    public function setLocalReceiptDateTime(?\DateTimeImmutable $localReceiptDateTime): static
    {
        $this->localReceiptDateTime = $localReceiptDateTime;

        return $this;
    }

    /**
     * @return Collection<RefundInterface>
     */
    public function getRefunds(): Collection
    {
        return $this->refunds;
    }

    /**
     * @param Uuid $id
     */
    public function getRefundById(mixed $id): ?RefundInterface
    {
        return $this->refunds->findFirst(fn (int $index, RefundInterface $refund) => $refund->getId() === $id);
    }

    public function getRefundByPayuId(string $payuId): ?RefundInterface
    {
        return $this->refunds->findFirst(fn (int $index, RefundInterface $refund) => $refund->getPayuId() === $payuId);
    }

    public function addRefund(RefundInterface $refund): static
    {
        if (!$this->refunds->contains($refund)) {
            $this->refunds->add($refund);
        }

        if ($this !== $refund->getOrder()) {
            $refund->setOrder($this);
        }

        return $this;
    }

    public function removeRefund(RefundInterface $refund): static
    {
        if ($this->refunds->contains($refund)) {
            $this->refunds->removeElement($refund);
        }

        // Refund's order cannot be null - so it's not changed on the inverse side.

        return $this;
    }

    public function getSubscription(): ?SubscriptionInterface
    {
        return $this->subscription;
    }

    public function setSubscription(?SubscriptionInterface $subscription): static
    {
        $previousSubscription = $this->subscription;

        $this->subscription = $subscription;

        $previousSubscription?->removeOrder($this);

        $subscription->addOrder($this);

        return $this;
    }
}
