<?php

namespace XOne\Bundle\PayuBundle\Factory;

use XOne\Bundle\PayuBundle\Model\OrderInterface;
use XOne\Bundle\PayuBundle\Model\RefundInterface;

interface RefundFactoryInterface
{
    public function create(OrderInterface $order, ?int $amount = null, ?string $description = null): RefundInterface;
}
