<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Factory;

use XOne\Bundle\PayuBundle\Entity\Refund;
use XOne\Bundle\PayuBundle\Model\OrderInterface;
use XOne\Bundle\PayuBundle\Model\RefundInterface;

class RefundFactory implements RefundFactoryInterface
{
    public function __construct(
        private string $class = Refund::class,
    ) {
    }

    public function create(OrderInterface $order, ?int $amount = null, ?string $description = null): RefundInterface
    {
        return (new $this->class())
            ->setOrder($order)
            ->setAmount($amount)
            ->setDescription($description)
        ;
    }
}
