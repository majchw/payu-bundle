<?php

namespace XOne\Bundle\PayuBundle\Factory;

use XOne\Bundle\PayuBundle\Model\OrderInterface;

interface OrderFactoryInterface
{
    public function create(string $description, string $currencyCode, int $totalAmount): OrderInterface;
}
