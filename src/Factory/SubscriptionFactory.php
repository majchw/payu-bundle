<?php

namespace XOne\Bundle\PayuBundle\Factory;

use XOne\Bundle\PayuBundle\Entity\Subscription;
use XOne\Bundle\PayuBundle\Model\SubscriptionFrequencyType;
use XOne\Bundle\PayuBundle\Model\SubscriptionInterface;

class SubscriptionFactory implements SubscriptionFactoryInterface
{
    public function __construct(
        private string $class = Subscription::class,
    ) {
    }

    public function create(string $firstCardToken, SubscriptionFrequencyType $frequencyType, int $frequencyValue = 1): SubscriptionInterface
    {
        return (new $this->class())
            ->setFirstCardToken($firstCardToken)
            ->setFrequencyType($frequencyType)
            ->setFrequencyValue($frequencyValue)
        ;
    }
}
