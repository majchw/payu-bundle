<?php

namespace XOne\Bundle\PayuBundle\Factory;

use XOne\Bundle\PayuBundle\Model\SubscriptionFrequencyType;
use XOne\Bundle\PayuBundle\Model\SubscriptionInterface;

interface SubscriptionFactoryInterface
{
    public function create(string $firstCardToken, SubscriptionFrequencyType $frequencyType, int $frequencyValue = 1): SubscriptionInterface;
}
