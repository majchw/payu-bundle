<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Factory;

use XOne\Bundle\PayuBundle\Entity\Order;
use XOne\Bundle\PayuBundle\Model\OrderInterface;

class OrderFactory implements OrderFactoryInterface
{
    public function __construct(
        private string $class = Order::class,
    ) {
    }

    public function create(string $description, string $currencyCode, int $totalAmount): OrderInterface
    {
        return (new $this->class())
            ->setDescription($description)
            ->setCurrencyCode($currencyCode)
            ->setTotalAmount($totalAmount)
        ;
    }
}
