<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Http;

/**
 * Wraps {@see \OpenPayU_Order} and {@see \OpenPayU_Refund} in a non-static class for easier mocking in tests.
 */
interface OpenPayuInterface
{
    public function createOrder(array $order): \OpenPayU_Result;

    public function getOrderRefunds(string $orderId): ?\OpenPayU_Result;

    public function consumeOrderNotification(string $data): ?\OpenPayU_Result;

    public function createRefund(array $refund): ?\OpenPayU_Result;
}
