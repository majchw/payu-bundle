<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Http;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use XOne\Bundle\PayuBundle\Event\OrderNotificationEvent;
use XOne\Bundle\PayuBundle\Model\OrderInterface;
use XOne\Bundle\PayuBundle\Model\RefundInterface;
use XOne\Bundle\PayuBundle\Model\RefundResponse;
use XOne\Bundle\PayuBundle\Repository\OrderRepositoryInterface;

class Client implements ClientInterface
{
    private ?LoggerInterface $logger = null;

    public function __construct(
        private ClientConfigInterface $config,
        private OrderRepositoryInterface $orderRepository,
        private EventDispatcherInterface $eventDispatcher,
        private UrlGeneratorInterface $urlGenerator,
        private RequestStack $requestStack,
        private OpenPayuInterface $openPayu = new OpenPayu(),
    ) {
    }

    public function setLogger(?LoggerInterface $logger = null): self
    {
        $this->logger = $logger;

        return $this;
    }

    public function createOrder(OrderInterface $order): OrderInterface
    {
        $this->config->apply();

        $orderData = [
            'extOrderId' => (string) $order->getId(),
            'description' => $order->getDescription(),
            'currencyCode' => $order->getCurrencyCode(),
            'totalAmount' => (string) $order->getTotalAmount(),
            'merchantPosId' => $this->config->getMerchantPosId(),
            'continueUrl' => $this->urlGenerator->generate(
                name: $this->config->getContinueRoute(),
                parameters: ['order' => $order->getId()],
                referenceType: UrlGeneratorInterface::ABSOLUTE_URL,
            ),
            'notifyUrl' => $this->urlGenerator->generate(
                name: $this->config->getNotifyRoute(),
                parameters: ['order' => $order->getId()],
                referenceType: UrlGeneratorInterface::ABSOLUTE_URL,
            ),
        ];

        // Optional order data - filtered for valid structure
        $orderData = array_replace_recursive($orderData, array_filter([
            'customerIp' => $this->requestStack->getCurrentRequest()?->getClientIp() ?? '127.0.0.1',
            'visibleDescription' => $order->getVisibleDescription(),
            'additionalDescription' => $order->getAdditionalDescription(),
            'statementDescription' => $order->getStatementDescription(),
            'buyer' => array_filter([
                'firstName' => $order->getBuyerFirstName(),
                'lastName' => $order->getBuyerLastName(),
                'email' => $order->getBuyerEmail(),
                'language' => $order->getBuyerLanguage(),
            ]),
        ]));

        if ($subscription = $order->getSubscription()) {
            $orderData = array_replace_recursive($orderData, [
                'recurring' => null === $subscription->getReusableCardToken() ? 'FIRST' : 'STANDARD',
                'threeDsAuthentication' => [
                    'recurring' => [
                        'frequency' => 1,
                        'expiry' => '9999-12-31T00:00:00Z',
                    ],
                ],
                'payMethods' => [
                    'payMethod' => [
                        'type' => 'CARD_TOKEN',
                        'value' => $subscription->getReusableCardToken() ?? $subscription->getFirstCardToken(),
                    ],
                ],
            ]);
        }

        $result = $this->openPayu->createOrder($orderData);

        $response = $result->getResponse();

        $order->setPayuId($response->orderId);

        if ($redirectUri = ($response->redirectUri ?? null)) {
            $order->setRedirectUri($redirectUri);
        }

        if ($subscription) {
            $payMethod = $response->payMethods->payMethod ?? null;

            if (!empty($reusableCardToken = $payMethod->value ?? null)) {
                $subscription->setReusableCardToken($reusableCardToken);
            }

            $cardDetails = $payMethod->card ?? null;

            if (!empty($cardNumber = $cardDetails->number ?? null)) {
                $subscription->setCardNumber($cardNumber);
            }

            if (!empty($cardExpirationMonth = $cardDetails->expirationMonth ?? null)) {
                $subscription->setCardExpirationMonth($cardExpirationMonth);
            }

            if (!empty($cardExpirationYear = $cardDetails->expirationYear ?? null)) {
                $subscription->setCardExpirationYear($cardExpirationYear);
            }
        }

        return $order;
    }

    public function getOrderRefunds(OrderInterface $order): array
    {
        $this->config->apply();

        $result = $this->openPayu->getOrderRefunds((string) $order->getPayuId());

        $response = $result->getResponse();

        $refunds = [];

        if (is_iterable($response->refunds ?? null)) {
            foreach ($response->refunds as $refund) {
                $refunds[] = RefundResponse::fromResponse($refund);
            }
        }

        return $refunds;
    }

    public function consumeOrderNotification(?string $data = null): void
    {
        $this->config->apply();

        $data ??= trim(file_get_contents('php://input'));

        $this->logger?->info('Order notification received', ['data' => $data]);

        $result = $this->openPayu->consumeOrderNotification($data);

        if (null === $result) {
            $this->logger?->warning('Order notification consumed - got null instead of valid response');

            return;
        }

        $response = $result->getResponse();

        if (null === $response->order) {
            $this->logger?->warning('Order notification consumed - response order is null', ['data' => $data]);

            return;
        }

        if (!isset($response->order->extOrderId)) {
            $this->logger?->warning('Order notification consumed - response extOrderId is not set', ['data' => $data]);

            return;
        }

        if (!$this->eventDispatcher->hasListeners(OrderNotificationEvent::class)) {
            $this->logger?->info('Order notification consumed - no listeners are registered for the event', ['data' => $data]);

            return;
        }

        $order = $this->orderRepository->find($response->order->extOrderId);

        if (null === $order) {
            $this->logger?->warning('Order notification consumed - order not found', [
                'extOrderId' => $response->order->extOrderId,
                'data' => $data,
            ]);

            return;
        }

        $event = new OrderNotificationEvent($order, $result);

        $this->eventDispatcher->dispatch($event);

        $this->logger?->info('Order notification consumed - event dispatched', ['data' => $data]);
    }

    public function createRefund(RefundInterface $refund): RefundInterface
    {
        $this->config->apply();

        $result = $this->openPayu->createRefund([
            'orderId' => (string) $refund->getOrder()->getPayuId(),
            'extRefundId' => (string) $refund->getId(),
            'amount' => $refund->getAmount(),
            'currencyCode' => $refund->getCurrencyCode(),
            'description' => $refund->getDescription(),
        ]);

        $response = $result->getResponse();

        $refund->setPayuId($response->refund->refundId);

        return $refund;
    }
}
