<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Http;

use XOne\Bundle\PayuBundle\Exception\InvalidArgumentException;

class OpenPayu implements OpenPayuInterface
{
    /**
     * @throws \OpenPayU_Exception
     */
    public function createOrder(array $order): \OpenPayU_Result
    {
        return \OpenPayU_Order::create($order);
    }

    /**
     * @throws \OpenPayU_Exception
     */
    public function getOrderRefunds(string $orderId): ?\OpenPayU_Result
    {
        return \OpenPayU_Order::retrieveRefund($orderId);
    }

    /**
     * @throws \OpenPayU_Exception
     */
    public function consumeOrderNotification(string $data): ?\OpenPayU_Result
    {
        return \OpenPayU_Order::consumeNotification($data);
    }

    /**
     * @throws \OpenPayU_Exception
     */
    public function createRefund(array $refund): ?\OpenPayU_Result
    {
        if (empty($refund['orderId'])) {
            throw new InvalidArgumentException('The refund\'s order identifier must be given and cannot be empty');
        }

        if (empty($refund['description'])) {
            throw new InvalidArgumentException('The refund\'s description must be given and cannot be empty');
        }

        return \OpenPayU_Refund::create(
            orderId: $refund['orderId'],
            description: $refund['description'],
            amount: $refund['amount'] ?? null,
            extCustomerId: $refund['extCustomerId'] ?? null,
            extRefundId: $refund['extRefundId'] ?? null,
        );
    }
}
