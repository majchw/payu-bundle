<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Http;

use XOne\Bundle\PayuBundle\Model\OrderInterface;
use XOne\Bundle\PayuBundle\Model\RefundInterface;
use XOne\Bundle\PayuBundle\Model\RefundResponse;

interface ClientInterface
{
    /**
     * Creates an order in PayU.
     *
     * @param OrderInterface $order order to create using the PayU API
     *
     * @return OrderInterface order filled with data returned by PayU API (e.g. identifier, redirect uri)
     *
     * @throws \OpenPayU_Exception
     */
    public function createOrder(OrderInterface $order): OrderInterface;

    /**
     * Retrieves refunds created for given order from PayU.
     *
     * @param OrderInterface $order order to retrieve refunds for
     *
     * @return array<RefundResponse> an array of refund response DTOs
     *
     * @throws \OpenPayU_Exception
     */
    public function getOrderRefunds(OrderInterface $order): array;

    /**
     * Consumes an order notification from PayU.
     *
     * @param string|null $data if null, the data is read from php://input stream
     *
     * @throws \OpenPayU_Exception
     */
    public function consumeOrderNotification(?string $data = null): void;

    /**
     * Creates a refund in PayU.
     *
     * @param RefundInterface $refund refund to create using the PayU API
     *
     * @return RefundInterface refund filled with data returned by PayU API (e.g. identifier)
     *
     * @throws \OpenPayU_Exception
     */
    public function createRefund(RefundInterface $refund): RefundInterface;
}
