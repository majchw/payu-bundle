<?php

namespace XOne\Bundle\PayuBundle\Http;

interface ClientConfigInterface
{
    public function getEnvironment(): string;

    public function getMerchantPosId(): string;

    public function getSignatureKey(): string;

    public function getOauthClientId(): string;

    public function getOauthClientSecret(): string;

    public function getOauthGrantType(): string;

    public function getOauthEmail(): ?string;

    public function getOauthExtCustomerId(): ?string;

    public function getContinueRoute(): string;

    public function getNotifyRoute(): string;

    public function getCacheDir(): string;

    public function apply(): void;
}
