<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Http;

class ClientConfig implements ClientConfigInterface
{
    public function __construct(
        private string $environment,
        private string $merchantPosId,
        #[\SensitiveParameter] private string $signatureKey,
        private string $oauthClientId,
        #[\SensitiveParameter] private string $oauthClientSecret,
        private string $oauthGrantType,
        private ?string $oauthEmail,
        private ?string $oauthExtCustomerId,
        private string $continueRoute,
        private string $notifyRoute,
        private string $cacheDir,
    ) {
    }

    public function getEnvironment(): string
    {
        return $this->environment;
    }

    public function getMerchantPosId(): string
    {
        return $this->merchantPosId;
    }

    public function getSignatureKey(): string
    {
        return $this->signatureKey;
    }

    public function getOauthClientId(): string
    {
        return $this->oauthClientId;
    }

    public function getOauthClientSecret(): string
    {
        return $this->oauthClientSecret;
    }

    public function getOauthGrantType(): string
    {
        return $this->oauthGrantType;
    }

    public function getOauthEmail(): ?string
    {
        return $this->oauthEmail;
    }

    public function getOauthExtCustomerId(): ?string
    {
        return $this->oauthExtCustomerId;
    }

    public function getContinueRoute(): string
    {
        return $this->continueRoute;
    }

    public function getNotifyRoute(): string
    {
        return $this->notifyRoute;
    }

    public function getCacheDir(): string
    {
        return $this->cacheDir;
    }

    /**
     * @throws \OpenPayU_Exception_Configuration
     */
    public function apply(): void
    {
        \OpenPayU_Configuration::setEnvironment($this->environment);
        \OpenPayU_Configuration::setMerchantPosId($this->merchantPosId);
        \OpenPayU_Configuration::setSignatureKey($this->signatureKey);
        \OpenPayu_Configuration::setOauthClientId($this->oauthClientId);
        \OpenPayu_Configuration::setOauthClientSecret($this->oauthClientSecret);
        \OpenPayu_Configuration::setOauthGrantType($this->oauthGrantType);
        \OpenPayu_Configuration::setOauthEmail($this->oauthEmail);
        \OpenPayu_Configuration::setOauthExtCustomerId($this->oauthExtCustomerId);
        \OpenPayU_Configuration::setOauthTokenCache(new \OauthCacheFile($this->cacheDir));
    }
}
