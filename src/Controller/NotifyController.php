<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use XOne\Bundle\PayuBundle\Http\ClientInterface;

class NotifyController
{
    public function __construct(
        private ClientInterface $client,
    ) {
    }

    public function __invoke(): Response
    {
        $this->client->consumeOrderNotification();

        return new Response();
    }
}
