<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Exception;

interface ExceptionInterface extends \Throwable
{
}
