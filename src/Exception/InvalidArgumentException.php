<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}
