<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Exception;

class BadMethodCallException extends \BadMethodCallException implements ExceptionInterface
{
}
