<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Tests\Http;

use PHPUnit\Framework\TestCase;
use XOne\Bundle\PayuBundle\Http\ClientConfig;

class ClientConfigTest extends TestCase
{
    public function testApply()
    {
        $config = new ClientConfig(
            environment: 'sandbox',
            merchantPosId: '512163',
            signatureKey: 'ce52790629679d930ca16c39a4f619c3',
            oauthClientId: '648128',
            oauthClientSecret: '367b77cd3245a3aa7f9b986d0bd31ad7',
            oauthGrantType: 'trusted_merchant',
            oauthEmail: 'test@example.com',
            oauthExtCustomerId: '8c16551b-0fb6-4e79-a335-4f092874ed7f',
            continueRoute: 'app_payment_payu_continue',
            notifyRoute: 'app_payment_payu_notify',
            cacheDir: __DIR__,
        );

        $config->apply();

        $this->assertSame('sandbox', \OpenPayU_Configuration::getEnvironment());
        $this->assertSame('512163', \OpenPayU_Configuration::getMerchantPosId());
        $this->assertSame('ce52790629679d930ca16c39a4f619c3', \OpenPayU_Configuration::getSignatureKey());
        $this->assertSame('648128', \OpenPayU_Configuration::getOauthClientId());
        $this->assertSame('367b77cd3245a3aa7f9b986d0bd31ad7', \OpenPayU_Configuration::getOauthClientSecret());
        $this->assertSame('trusted_merchant', \OpenPayU_Configuration::getOauthGrantType());
        $this->assertSame('test@example.com', \OpenPayU_Configuration::getOauthEmail());
        $this->assertSame('8c16551b-0fb6-4e79-a335-4f092874ed7f', \OpenPayU_Configuration::getOauthExtCustomerId());
        $this->assertEquals(new \OauthCacheFile(__DIR__), \OpenPayU_Configuration::getOauthTokenCache());
    }
}
