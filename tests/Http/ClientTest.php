<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Tests\Http;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Uid\Uuid;
use XOne\Bundle\PayuBundle\Entity\Order;
use XOne\Bundle\PayuBundle\Entity\Refund;
use XOne\Bundle\PayuBundle\Entity\Subscription;
use XOne\Bundle\PayuBundle\Event\OrderNotificationEvent;
use XOne\Bundle\PayuBundle\Http\Client;
use XOne\Bundle\PayuBundle\Http\ClientConfigInterface;
use XOne\Bundle\PayuBundle\Http\OpenPayuInterface;
use XOne\Bundle\PayuBundle\Model\RefundResponse;
use XOne\Bundle\PayuBundle\Model\SubscriptionFrequencyType;
use XOne\Bundle\PayuBundle\Repository\OrderRepositoryInterface;
use XOne\Bundle\PayuBundle\Tests\ReflectionTrait;

class ClientTest extends TestCase
{
    use ReflectionTrait;

    public function testCreateOrder()
    {
        $order = $this->createOrder(id: $orderId = Uuid::v4());

        $client = $this->createClient(
            config: $this->createClientConfigMock(merchantPosId: '172759'),
            urlGenerator: $this->createUrlGeneratorMock(url: 'https://app.local'),
            requestStack: $this->createRequestStackMock(currentRequestClientIp: '127.0.0.1'),
            openPayu: $this->createOpenPayuMock(
                expectedOrderData: [
                    'extOrderId' => (string) $orderId,
                    'description' => 'Lorem ipsum dolor sit amet',
                    'additionalDescription' => 'Aliquam vitae lorem non',
                    'visibleDescription' => 'Aenean laoreet lacus eget',
                    'statementDescription' => 'Nulla facilisi',
                    'currencyCode' => 'PLN',
                    'totalAmount' => 10_00, // 10 zł
                    'customerIp' => '127.0.0.1',
                    'merchantPosId' => '172759',
                    'continueUrl' => 'https://app.local',
                    'notifyUrl' => 'https://app.local',
                    'buyer' => [
                        'firstName' => 'John',
                        'lastName' => 'Doe',
                        'email' => 'john-doe@example.com',
                        'language' => 'en',
                    ],
                ],
                result: $this->createOpenPayuResult((object) [
                    'orderId' => 'foo',
                    'redirectUri' => 'bar',
                ])
            ),
        );

        $client->createOrder($order);

        $this->assertSame('foo', $order->getPayuId());
        $this->assertSame('bar', $order->getRedirectUri());
    }

    public function testCreateFirstRecurringOrder()
    {
        $order = $this->createOrder(id: $orderId = Uuid::v4());
        $order->setSubscription($this->createSubscription(firstCardToken: 'TOK_1LJRPX2LQMRU95G3Fyl9uKwUf75E'));

        $client = $this->createClient(
            config: $this->createClientConfigMock(merchantPosId: '172759'),
            urlGenerator: $this->createUrlGeneratorMock(url: 'https://app.local'),
            requestStack: $this->createRequestStackMock(currentRequestClientIp: '127.0.0.1'),
            openPayu: $this->createOpenPayuMock(
                expectedOrderData: [
                    'extOrderId' => (string) $orderId,
                    'description' => 'Lorem ipsum dolor sit amet',
                    'additionalDescription' => 'Aliquam vitae lorem non',
                    'visibleDescription' => 'Aenean laoreet lacus eget',
                    'statementDescription' => 'Nulla facilisi',
                    'currencyCode' => 'PLN',
                    'totalAmount' => 10_00, // 10 zł
                    'customerIp' => '127.0.0.1',
                    'merchantPosId' => '172759',
                    'continueUrl' => 'https://app.local',
                    'notifyUrl' => 'https://app.local',
                    'buyer' => [
                        'firstName' => 'John',
                        'lastName' => 'Doe',
                        'email' => 'john-doe@example.com',
                        'language' => 'en',
                    ],
                    'recurring' => 'FIRST',
                    'threeDsAuthentication' => [
                        'recurring' => [
                            'frequency' => 1,
                            'expiry' => '9999-12-31T00:00:00Z',
                        ],
                    ],
                    'payMethods' => [
                        'payMethod' => [
                            'type' => 'CARD_TOKEN',
                            'value' => 'TOK_1LJRPX2LQMRU95G3Fyl9uKwUf75E',
                        ],
                    ],
                ],
                result: $this->createOpenPayuResult((object) [
                    'orderId' => 'foo',
                    'redirectUri' => 'bar',
                    'payMethods' => (object) [
                        'payMethod' => (object) [
                            'card' => (object) [
                                'number' => '444433******1111',
                                'expirationMonth' => 1,
                                'expirationYear' => 2028,
                            ],
                            'type' => 'CARD_TOKEN',
                            'value' => 'TOKC_1J19GJs9192hJSJ4hf929PWMs62P',
                        ],
                    ],
                ]),
            ),
        );

        $client->createOrder($order);

        $this->assertSame('foo', $order->getPayuId());
        $this->assertSame('bar', $order->getRedirectUri());
        $this->assertSame('TOKC_1J19GJs9192hJSJ4hf929PWMs62P', $order->getSubscription()->getReusableCardToken());
        $this->assertSame('444433******1111', $order->getSubscription()->getCardNumber());
        $this->assertSame(1, $order->getSubscription()->getCardExpirationMonth());
        $this->assertSame(2028, $order->getSubscription()->getCardExpirationYear());
    }

    public function testCreateStandardRecurringOrder()
    {
        $order = $this->createOrder(id: $orderId = Uuid::v4());
        $order->setSubscription($this->createSubscription(
            firstCardToken: 'TOK_1LJRPX2LQMRU95G3Fyl9uKwUf75E',
            reusableCardToken: 'TOKC_1J19GJs9192hJSJ4hf929PWMs62P',
        ));

        $client = $this->createClient(
            config: $this->createClientConfigMock(merchantPosId: '172759'),
            urlGenerator: $this->createUrlGeneratorMock(url: 'https://app.local'),
            requestStack: $this->createRequestStackMock(currentRequestClientIp: '127.0.0.1'),
            openPayu: $this->createOpenPayuMock(
                expectedOrderData: [
                    'extOrderId' => (string) $orderId,
                    'description' => 'Lorem ipsum dolor sit amet',
                    'additionalDescription' => 'Aliquam vitae lorem non',
                    'visibleDescription' => 'Aenean laoreet lacus eget',
                    'statementDescription' => 'Nulla facilisi',
                    'currencyCode' => 'PLN',
                    'totalAmount' => 10_00, // 10 zł
                    'customerIp' => '127.0.0.1',
                    'merchantPosId' => '172759',
                    'continueUrl' => 'https://app.local',
                    'notifyUrl' => 'https://app.local',
                    'buyer' => [
                        'firstName' => 'John',
                        'lastName' => 'Doe',
                        'email' => 'john-doe@example.com',
                        'language' => 'en',
                    ],
                    'recurring' => 'STANDARD',
                    'threeDsAuthentication' => [
                        'recurring' => [
                            'frequency' => 1,
                            'expiry' => '9999-12-31T00:00:00Z',
                        ],
                    ],
                    'payMethods' => [
                        'payMethod' => [
                            'type' => 'CARD_TOKEN',
                            // Important: expect usage of reusable card token TOKC instead of first card token TOK
                            'value' => 'TOKC_1J19GJs9192hJSJ4hf929PWMs62P',
                        ],
                    ],
                ],
                result: $this->createOpenPayuResult((object) [
                    'orderId' => 'foo',
                    'redirectUri' => 'bar',
                ]),
            ),
        );

        $client->createOrder($order);

        $this->assertNull($order->getSubscription()->getCardNumber());
        $this->assertNull($order->getSubscription()->getCardExpirationMonth());
        $this->assertNull($order->getSubscription()->getCardExpirationYear());
    }

    public function testConsumeOrderNotification()
    {
        $data = 'foo=bar&baz=qux';

        $result = new \OpenPayU_Result();
        $result->setResponse((object) [
            'order' => (object) [
                'extOrderId' => '123',
            ],
        ]);

        $openPayu = $this->createMock(OpenPayuInterface::class);
        $openPayu->expects($this->once())->method('consumeOrderNotification')->with($data)->willReturn($result);

        $order = new Order();

        $orderRepository = $this->createMock(OrderRepositoryInterface::class);
        $orderRepository->expects($this->once())->method('find')->with('123')->willReturn($order);

        $eventDispatcher = new EventDispatcher();
        $eventDispatcher->addListener(OrderNotificationEvent::class, function (OrderNotificationEvent $event) use ($order, $result) {
            $this->assertSame($order, $event->getOrder());
            $this->assertSame($result, $event->getOpenPayuResult());
        });

        $client = $this->createClient(orderRepository: $orderRepository, eventDispatcher: $eventDispatcher, openPayu: $openPayu);
        $client->consumeOrderNotification($data);
    }

    public function testCreateRefund()
    {
        $order = new Order();

        $orderPayuId = Uuid::v4();

        $this->setPrivatePropertyValue($order, 'payuId', $orderPayuId);

        $refund = (new Refund())
            ->setOrder($order)
            ->setDescription('Lorem ipsum dolor sit amet')
            ->setAmount(10_00)
            ->setCurrencyCode('EUR')
        ;

        $refundId = Uuid::v4();

        $this->setPrivatePropertyValue($refund, 'id', $refundId);

        $openPayuResult = new \OpenPayU_Result();
        $openPayuResult->setResponse((object) [
            'refund' => (object) [
                'refundId' => 'foo',
            ],
        ]);

        $openPayu = $this->createMock(OpenPayuInterface::class);
        $openPayu->expects($this->once())->method('createRefund')->with([
            'orderId' => (string) $orderPayuId,
            'extRefundId' => (string) $refundId,
            'description' => 'Lorem ipsum dolor sit amet',
            'amount' => 10_00,
            'currencyCode' => 'EUR',
        ])->willReturn($openPayuResult);

        $client = $this->createClient(openPayu: $openPayu);
        $client->createRefund($refund);

        $this->assertSame('foo', $refund->getPayuId());
    }

    public function testCreateRefundWithoutAmount()
    {
        $order = new Order();

        $orderPayuId = Uuid::v4();

        $this->setPrivatePropertyValue($order, 'payuId', $orderPayuId);

        $refund = (new Refund())
            ->setOrder($order)
            ->setDescription('Lorem ipsum dolor sit amet')
            ->setCurrencyCode('EUR')
        ;

        $refundId = Uuid::v4();

        $this->setPrivatePropertyValue($refund, 'id', $refundId);

        $openPayuResult = new \OpenPayU_Result();
        $openPayuResult->setResponse((object) [
            'refund' => (object) [
                'refundId' => 'foo',
            ],
        ]);

        $openPayu = $this->createMock(OpenPayuInterface::class);
        $openPayu->expects($this->once())->method('createRefund')->with([
            'orderId' => (string) $orderPayuId,
            'extRefundId' => (string) $refundId,
            'description' => 'Lorem ipsum dolor sit amet',
            'amount' => null,
            'currencyCode' => 'EUR',
        ])->willReturn($openPayuResult);

        $client = $this->createClient(openPayu: $openPayu);
        $client->createRefund($refund);

        $this->assertSame('foo', $refund->getPayuId());
    }

    public function testGetOrderRefunds()
    {
        $order = new Order();

        $orderPayuId = Uuid::v4();

        $this->setPrivatePropertyValue($order, 'payuId', $orderPayuId);

        $openPayuResult = new \OpenPayU_Result();
        $openPayuResult->setResponse((object) [
            'refunds' => [
                (object) [
                    'refundId' => '5000716758',
                    'extRefundId' => 'd88eaf10-a3dd-401b-bcd3-4cac5fa77be6',
                    'amount' => '1',
                    'currencyCode' => 'PLN',
                    'description' => 'Zwrot',
                    'creationDateTime' => '2024-08-30T14:39:40.167+02:00',
                    'statusDateTime' => '2024-08-30T14:39:50.848+02:00',
                    'status' => 'FINALIZED',
                ],
                (object) [
                    'refundId' => '5000716759',
                    'extRefundId' => '0191a36e-7a2a-765d-977b-1f6f8f1cc675',
                    'amount' => '2',
                    'currencyCode' => 'EUR',
                    'description' => 'Refund',
                    'creationDateTime' => '2024-08-30T14:39:40.167+02:00',
                    'statusDateTime' => '2024-08-30T14:39:50.848+02:00',
                    'status' => 'CANCELED',
                    'statusError' => (object) [
                        'code' => 'foo',
                        'description' => 'bar',
                    ],
                ],
            ],
        ]);

        $openPayu = $this->createMock(OpenPayuInterface::class);
        $openPayu->expects($this->once())->method('getOrderRefunds')
            ->with($order->getPayuId())
            ->willReturn($openPayuResult);

        $client = $this->createClient(openPayu: $openPayu);

        $refunds = $client->getOrderRefunds($order);

        $this->assertCount(2, $refunds);

        $this->assertInstanceOf(RefundResponse::class, $refunds[0]);
        $this->assertSame('d88eaf10-a3dd-401b-bcd3-4cac5fa77be6', $refunds[0]->getId());
        $this->assertSame('5000716758', $refunds[0]->getPayuId());
        $this->assertSame(1, $refunds[0]->getAmount());
        $this->assertSame('PLN', $refunds[0]->getCurrencyCode());
        $this->assertSame('Zwrot', $refunds[0]->getDescription());
        $this->assertSame('FINALIZED', $refunds[0]->getStatus());
        $this->assertNull($refunds[0]->getStatusErrorCode());
        $this->assertNull($refunds[0]->getStatusErrorDescription());

        $this->assertInstanceOf(RefundResponse::class, $refunds[1]);
        $this->assertSame('0191a36e-7a2a-765d-977b-1f6f8f1cc675', $refunds[1]->getId());
        $this->assertSame('5000716759', $refunds[1]->getPayuId());
        $this->assertSame(2, $refunds[1]->getAmount());
        $this->assertSame('EUR', $refunds[1]->getCurrencyCode());
        $this->assertSame('Refund', $refunds[1]->getDescription());
        $this->assertSame('CANCELED', $refunds[1]->getStatus());
        $this->assertSame('foo', $refunds[1]->getStatusErrorCode());
        $this->assertSame('bar', $refunds[1]->getStatusErrorDescription());
    }

    public function testGetOrderRefundsWithNoRefunds()
    {
        $order = new Order();

        $orderPayuId = Uuid::v4();

        $this->setPrivatePropertyValue($order, 'payuId', $orderPayuId);

        $openPayuResult = new \OpenPayU_Result();
        $openPayuResult->setResponse((object) [
            'refunds' => false,
        ]);

        $openPayu = $this->createMock(OpenPayuInterface::class);
        $openPayu->expects($this->once())->method('getOrderRefunds')
            ->with($order->getPayuId())
            ->willReturn($openPayuResult);

        $client = $this->createClient(openPayu: $openPayu);

        $this->assertEmpty($client->getOrderRefunds($order));
    }

    private function createClient(
        ?ClientConfigInterface $config = null,
        ?OrderRepositoryInterface $orderRepository = null,
        ?EventDispatcherInterface $eventDispatcher = null,
        ?UrlGeneratorInterface $urlGenerator = null,
        ?RequestStack $requestStack = null,
        ?OpenPayuInterface $openPayu = null,
    ): Client {
        $config ??= $this->createStub(ClientConfigInterface::class);
        $orderRepository ??= $this->createStub(OrderRepositoryInterface::class);
        $eventDispatcher ??= $this->createStub(EventDispatcherInterface::class);
        $urlGenerator ??= $this->createStub(UrlGeneratorInterface::class);
        $requestStack ??= $this->createStub(RequestStack::class);
        $openPayu ??= $this->createStub(OpenPayuInterface::class);

        return new Client($config, $orderRepository, $eventDispatcher, $urlGenerator, $requestStack, $openPayu);
    }

    private function createOrder(?Uuid $id = null): Order
    {
        $order = (new Order())
            ->setDescription('Lorem ipsum dolor sit amet')
            ->setAdditionalDescription('Aliquam vitae lorem non')
            ->setVisibleDescription('Aenean laoreet lacus eget')
            ->setStatementDescription('Nulla facilisi')
            ->setCurrencyCode('PLN')
            ->setTotalAmount(10_00) // 10 zł
            ->setBuyerFirstName('John')
            ->setBuyerLastName('Doe')
            ->setBuyerEmail('john-doe@example.com')
            ->setBuyerLanguage('en')
        ;

        if (null !== $id) {
            $this->setPrivatePropertyValue($order, 'id', $id);
        }

        return $order;
    }

    private function createSubscription(string $firstCardToken, ?string $reusableCardToken = null): Subscription
    {
        return (new Subscription())
            ->setFrequencyType(SubscriptionFrequencyType::Monthly)
            ->setFrequencyValue(1)
            ->setFirstCardToken($firstCardToken)
            ->setReusableCardToken($reusableCardToken)
        ;
    }

    private function createRequestStackMock(string $currentRequestClientIp): MockObject&RequestStack
    {
        $request = $this->createMock(Request::class);
        $request->method('getClientIp')->willReturn($currentRequestClientIp);

        $requestStack = $this->createMock(RequestStack::class);
        $requestStack->method('getCurrentRequest')->willReturn($request);

        return $requestStack;
    }

    private function createUrlGeneratorMock(string $url): MockObject&UrlGeneratorInterface
    {
        $urlGenerator = $this->createMock(UrlGeneratorInterface::class);
        $urlGenerator->method('generate')->willReturn($url);

        return $urlGenerator;
    }

    private function createClientConfigMock(string $merchantPosId): MockObject&ClientConfigInterface
    {
        $config = $this->createMock(ClientConfigInterface::class);
        $config->method('getMerchantPosId')->willReturn($merchantPosId);

        return $config;
    }

    private function createOpenPayuMock(array $expectedOrderData, \OpenPayU_Result $result): MockObject&OpenPayuInterface
    {
        $openPayu = $this->createMock(OpenPayuInterface::class);
        $openPayu->expects($this->once())->method('createOrder')->with($expectedOrderData)->willReturn($result);

        return $openPayu;
    }

    private function createOpenPayuResult(object $response): \OpenPayU_Result
    {
        $openPayuResult = new \OpenPayU_Result();
        $openPayuResult->setResponse($response);

        return $openPayuResult;
    }
}
