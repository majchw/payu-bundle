<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Tests;

trait ReflectionTrait
{
    private static function getPrivatePropertyValue(object $object, string $property): mixed
    {
        $reflection = new \ReflectionProperty($object, $property);
        $reflection->setAccessible(true);

        return $reflection->getValue($object);
    }

    private static function setPrivatePropertyValue(object $object, string $property, mixed $value): void
    {
        $reflection = new \ReflectionProperty($object, $property);
        $reflection->setAccessible(true);
        $reflection->setValue($object, $value);
    }
}
