<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Tests\EventListener;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use XOne\Bundle\PayuBundle\Entity\Order;
use XOne\Bundle\PayuBundle\Event\OrderNotificationEvent;
use XOne\Bundle\PayuBundle\EventListener\UpdateLocalReceiptDateTimeFromNotification;
use XOne\Bundle\PayuBundle\Repository\OrderRepositoryInterface;

class UpdateLocalReceiptDateTimeFromNotificationTest extends TestCase
{
    /**
     * @dataProvider provideTestInvokeCases
     */
    public function testInvoke(object $response, ?\DateTimeImmutable $expected)
    {
        $order = new Order();

        $result = new \OpenPayU_Result();
        $result->setResponse($response);

        $orderRepository = $this->createMock(OrderRepositoryInterface::class);
        $orderRepository->expects($expected ? $this->once() : $this->never())->method('save')->with($order);

        $listener = new UpdateLocalReceiptDateTimeFromNotification($orderRepository);
        $listener(new OrderNotificationEvent($order, $result));

        $this->assertEquals($expected, $order->getLocalReceiptDateTime());
    }

    public static function provideTestInvokeCases(): iterable
    {
        yield 'Date time in "Y-m-d\TH:i:s.uP" format' => [
            (object) ['localReceiptDateTime' => '2024-09-30T10:29:53.731+02:00'],
            \DateTimeImmutable::createFromFormat('Y-m-d\TH:i:s.uP', '2024-09-30T10:29:53.731+02:00'),
        ];

        yield 'Date time in "Y-m-d" format' => [
            (object) ['localReceiptDateTime' => '2024-09-30'],
            null,
        ];

        yield 'Empty string instead of date time' => [
            (object) ['localReceiptDateTime' => ''],
            null,
        ];

        yield 'Null instead of date time' => [
            (object) ['localReceiptDateTime' => null],
            null,
        ];

        yield 'No date time' => [
            (object) [],
            null,
        ];
    }
}
