<?php

declare(strict_types=1);

namespace XOne\Bundle\PayuBundle\Tests\EventListener;

use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcher;
use XOne\Bundle\PayuBundle\Entity\Order;
use XOne\Bundle\PayuBundle\Event\OrderNotificationEvent;
use XOne\Bundle\PayuBundle\Event\OrderStatusChangeEvent;
use XOne\Bundle\PayuBundle\EventListener\UpdateOrderStatusFromNotification;
use XOne\Bundle\PayuBundle\Model\OrderStatus;
use XOne\Bundle\PayuBundle\Repository\OrderRepositoryInterface;

class UpdateOrderStatusFromNotificationTest extends TestCase
{
    /**
     * @dataProvider provideTestInvokeCases
     */
    public function testInvoke(object $response, ?OrderStatus $expected)
    {
        $order = new Order();
        $order->setStatus(OrderStatus::New);

        $result = new \OpenPayU_Result();
        $result->setResponse($response);

        $orderRepository = $this->createMock(OrderRepositoryInterface::class);
        $orderRepository->expects($expected ? $this->once() : $this->never())->method('save')->with($order);

        $eventDispatcher = $this->createMock(EventDispatcher::class);
        $eventDispatcher->expects($expected ? $this->once() : $this->never())->method('dispatch')->with(
            new OrderStatusChangeEvent($order, OrderStatus::New),
        );

        $listener = new UpdateOrderStatusFromNotification($orderRepository, $eventDispatcher);
        $listener(new OrderNotificationEvent($order, $result));

        $this->assertSame($expected ?? OrderStatus::New, $order->getStatus());
    }

    public static function provideTestInvokeCases(): iterable
    {
        yield 'Status "NEW"' => [
            (object) ['order' => (object) ['status' => 'NEW']],
            null,
        ];

        yield 'Status "PENDING"' => [
            (object) ['order' => (object) ['status' => 'PENDING']],
            OrderStatus::Pending,
        ];

        yield 'Status "WAITING_FOR_CONFIRMATION"' => [
            (object) ['order' => (object) ['status' => 'WAITING_FOR_CONFIRMATION']],
            OrderStatus::WaitingForConfirmation,
        ];

        yield 'Status "COMPLETED"' => [
            (object) ['order' => (object) ['status' => 'COMPLETED']],
            OrderStatus::Completed,
        ];

        yield 'Status "CANCELED"' => [
            (object) ['order' => (object) ['status' => 'CANCELED']],
            OrderStatus::Canceled,
        ];

        yield 'Status null' => [
            (object) ['order' => (object) ['status' => null]],
            null,
        ];

        yield 'Missing order status' => [
            (object) ['order' => (object) []],
            null,
        ];

        yield 'Missing order' => [
            (object) [],
            null,
        ];
    }
}
